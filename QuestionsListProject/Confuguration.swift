//
//  Confuguration.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 04.09.2021.
//

import Foundation

struct Configuration {

    static let baseDomain = "api.stackexchange.com/2.3"

    static let baseUrl: URL = {

        let urlString = "https://\(baseDomain)"
        guard let url = URL(string: urlString) else {
            fatalError("Unable to init URL for \(urlString)")
        }
        return url
    }()
}
