//
//  Answer.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 04.09.2021.
//

import Foundation

struct Answer: Decodable {
    let body: String?
    let isAccepted: Bool
    let owner: Owner
    let lastActivityDate: Double
    let score: Int?
}
