//
//  Owner.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 04.09.2021.
//

import Foundation

struct Owner: Decodable {
    let displayName: String
}
