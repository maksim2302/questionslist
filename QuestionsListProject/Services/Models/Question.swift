//
//  Question.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 04.09.2021.
//

import Foundation

struct Question: Decodable {
    let title: String
    let questionId: Int
    let body: String?
    let lastActivityDate: Double
    let answerCount: Int
    let answers: [Answer]?
    let owner: Owner
}
