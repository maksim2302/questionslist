//
//  QuestionsService.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 04.09.2021.
//

import Moya
import RxSwift

protocol QuestionsServiceProtocol {
    func getQuestions(pageSize: String, tag: String) -> Single<QuestionsResponse>
    func getQuestionDetails(id: Int) -> Single<QuestionsResponse>
}

final class QuestionsService: QuestionsServiceProtocol {

    // MARK: - Private Properties

    private let isLoggingEnabled = false

    private lazy var plugins: [NetworkLoggerPlugin] = {
        return isLoggingEnabled
            ? [NetworkLoggerPlugin(verbose: true, cURL: true)]
            : []
    }()

    private lazy var provider = MoyaProvider<QuestionsTarget>(plugins: plugins)

    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    // MARK: - Public Methods

    func getQuestions(pageSize: String, tag: String) -> Single<QuestionsResponse> {
        provider.rx.request(.questions(pageSize: pageSize, tag: tag))
            .map(QuestionsResponse.self, using: decoder)
    }

    func getQuestionDetails(id: Int) -> Single<QuestionsResponse> {
        provider.rx.request(.questionDetail(id: id))
            .map(QuestionsResponse.self, using: decoder)
    }
}
