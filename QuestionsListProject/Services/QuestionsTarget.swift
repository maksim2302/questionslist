//
//  QuestionsTarget.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 04.09.2021.
//

import Moya

enum QuestionsTarget {
    case questions(pageSize: String, tag: String)
    case questionDetail(id: Int)
}

extension QuestionsTarget: TargetType {

    var baseURL: URL { Configuration.baseUrl }

    var path: String {
        switch self {
        case .questions:
            return "/questions"
        case .questionDetail(let id):
            return "/questions/\(id)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .questions, .questionDetail:
            return .get
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {

        switch self {
        case .questions(let pageSize, let tag):
            let params: [String: String] = ["pagesize": pageSize, "tagged": tag, "site": "stackoverflow"]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .questionDetail:
            let params: [String: String] = ["filter": "!-*jbN-o8P3E5", "site": "stackoverflow"]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        return nil
    }
}
