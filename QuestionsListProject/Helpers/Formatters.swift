//
//  Formatters.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 13.09.2021.
//

import Foundation

final class Formatters {
    static let dateFormatterDefault: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
}
