//
//  Color.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 14.09.2021.
//

import UIKit

enum ProjectColor {
    /// #ECF8FF
    static let questionsCell = UIColor(hex: 0xECF8FF)
    /// #EEF1F7
    static let answersCell = UIColor(hex: 0xEEF1F7)
}

