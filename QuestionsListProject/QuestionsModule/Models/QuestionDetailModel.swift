//
//  QuestionDetailModel.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 12.09.2021.
//

import Foundation

struct QuestionDetailModel {
    let body: String?
    let date: Double
    let name: String
    let score: Int?
    let isAccepted: Bool
    let isAnswer: Bool
}
