//
//  QuestionsCoordinator.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 04.09.2021.
//

import RxSwift

final class QuestionsCoordinator {

    // MARK: - Private Properties

    private weak var navigationController: UINavigationController?
    private let service = QuestionsService()

    // MARK: - Public Methods

    func start() -> UINavigationController? {
        let viewModel = QuestionsListViewModel(service: service)
        let controller = QuestionsListVC(viewModel: viewModel)
        let navController = UINavigationController(rootViewController: controller)
        self.navigationController = navController
        bindQuestionsListViewModel(viewModel)
        return navController
    }

    // MARK: - Private Methods

    private func showSelectedQuestion(questionId: Int) {
        let vm = QuestionDetailViewModel(service: service, questionId: questionId)
        let controller = QuestionDetailVC(viewModel: vm)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func bindQuestionsListViewModel(_ model: QuestionsListViewModel) {
        model.onSelectedQuestion
            .bind { [weak self] id in
                self?.showSelectedQuestion(questionId: id)
            }
            .disposed(by: model.bag)
    }
}
