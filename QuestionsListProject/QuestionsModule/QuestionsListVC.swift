//
//  QuestionsListVC.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 07.09.2021.
//

import RxSwift

final class QuestionsListVC: UIViewController {

    // MARK: - Public Properties

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tagsPicker: UIPickerView!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!

    // MARK: - Private Properties

    private let viewModel: QuestionsListViewModel
    private let bag = DisposeBag()
    private let tags = ["Objective-C", "IOS", "Xcode", "Cocoa-Touch", "Iphone"]
    private var currentTag: String = "Objective-C"
    private var currentPageSize: Int = 20

    private lazy var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refreshTableView), for: .valueChanged)
        return refresh
    }()

    // MARK: - Initializers

    init(viewModel: QuestionsListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cicle

    override func viewDidLoad() {
        super.viewDidLoad()
        bindVM()
        bindUI()
        setupUI()
        getQuestionsForTag(currentTag, pageSize: currentPageSize)
        self.activityIndicatorView.startAnimating()
    }

    // MARK: - Private Methods

    private func setupUI() {
        tableView.register(UINib(nibName: "QuestionsListTableViewCell", bundle: nil), forCellReuseIdentifier: "QuestionsListCell")
        navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(shangeTags)), animated: true)
        setupTagsPicker()
        toolBar.isHidden = true
        tagsPicker.isHidden = true
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionsListCell", for: indexPath)
                if let cell = cell as? QuestionsListTableViewCell {
                    cell.configure(data)
                }
                tableView.deselectRow(at: indexPath, animated: true)
                return cell
            }
            .disposed(by: bag)

        tableView.rx
            .modelSelected(Question.self)
            .map { $0.questionId }
            .bind(to: viewModel.onSelectedQuestion)
            .disposed(by: bag)

        viewModel
            .loadFinished
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.activityIndicatorView.stopAnimating()
                self.navigationItem.title = self.currentTag
                if #available(iOS 10.0, *) {
                    self.tableView.refreshControl?.endRefreshing()
                } else {
                    self.tableView.addSubview(self.refreshControl)
                }
            }
            .disposed(by: bag)

        tableView.rx
            .reachedBottom()
            .skip(1)
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.currentPageSize += 20
                self.getQuestionsForTag(self.currentTag, pageSize: self.currentPageSize)
                self.activityIndicatorView.startAnimating()
            }
            .disposed(by: bag)
    }

    @objc private func shangeTags() {
        tagsPicker?.isHidden = false
        toolBar.isHidden = false
    }

    private func setupTagsPicker() {
        guard let tagsPicker = tagsPicker else { return }
        let pickerItems: Observable<[String]> = Observable.of(tags)
        pickerItems.bind(to: tagsPicker.rx.itemTitles) { row, element in
            return element
        }
        .disposed(by: bag)

        tagsPicker.rx.itemSelected
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .next(let selected):
                    self.currentTag = self.tags[selected.row]
                default:
                    break
                }
            }
            .disposed(by: bag)
    }

    private func getQuestionsForTag(_ tag: String, pageSize: Int) {
        viewModel.getQuestionsList(pageSize: pageSize, tag: tag)
    }

    private func bindUI() {
        doneButton.rx.tap
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.getQuestionsForTag(self.currentTag, pageSize: self.currentPageSize)
                self.activityIndicatorView.startAnimating()
                self.toolBar.isHidden = true
                self.tagsPicker.isHidden = true
            }
            .disposed(by: bag)
    }

    @objc private func refreshTableView() {
        getQuestionsForTag(currentTag, pageSize: currentPageSize)
    }
}
