//
//  QuestionDetailViewModel.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 04.09.2021.
//

import RxSwift
import RxCocoa

final class QuestionDetailViewModel {

    // MARK: - Public Properties

    private(set) var dataItems = PublishRelay<[QuestionDetailModel]>()

    // MARK: - Private properties

    private let service: QuestionsServiceProtocol
    private let questionId: Int
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(service: QuestionsServiceProtocol, questionId: Int) {
        self.service = service
        self.questionId = questionId
    }

    // MARK: - Public Methods

    func getQuestionDetails() {
        service.getQuestionDetails(id: questionId)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    var items = [QuestionDetailModel]()
                    guard let model = response.items.first(where: { $0.questionId == self.questionId }) else { return }
                    let question = QuestionDetailModel(body: model.body, date: model.lastActivityDate, name: model.owner.displayName, score: nil, isAccepted: false, isAnswer: false)
                    items.append(question)
                    let answers = model.answers?.map { QuestionDetailModel(body: $0.body, date: $0.lastActivityDate, name: $0.owner.displayName, score: $0.score, isAccepted: $0.isAccepted, isAnswer: true) }
                    items += answers ?? []
                    self.dataItems.accept(items)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }
}
