//
//  QuestionDetailVC.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 07.09.2021.
//

import RxSwift

class QuestionDetailVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    // MARK: - Private Properties

    private let viewModel: QuestionDetailViewModel
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: QuestionDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "QuestionDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "QuestionDetailCell")
        bindVM()
        viewModel.getQuestionDetails()
    }

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionDetailCell", for: indexPath)
                if let cell = cell as? QuestionDetailTableViewCell {
                    cell.configure(data)
                }
                tableView.deselectRow(at: indexPath, animated: true)
                return cell
            }
            .disposed(by: bag)
    }
}
