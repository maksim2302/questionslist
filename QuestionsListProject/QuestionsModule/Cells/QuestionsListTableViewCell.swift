//
//  QuestionsListTableViewCell.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 07.09.2021.
//

import UIKit

final class QuestionsListTableViewCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var answersCountLabel: UILabel!
    @IBOutlet weak var questionDateLabel: UILabel!

    func configure(_ model: Question) {
        questionLabel.text = model.title
        nameLabel.text = model.owner.displayName
        answersCountLabel.text = String(model.answerCount)
        if Date().timeIntervalSince1970 - model.lastActivityDate >= 86400 {
            questionDateLabel.text = Formatters.dateFormatterDefault.string(from: Date(timeIntervalSince1970: model.lastActivityDate))
        } else {
            questionDateLabel.text = "modified \(Date(timeIntervalSince1970: model.lastActivityDate).timeAgoSinceDate())"
        }
    }
}
