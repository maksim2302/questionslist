//
//  QuestionDetailTableViewCell.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 09.09.2021.
//

import UIKit

final class QuestionDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var votesCountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bodyTextView: UITextView!
    @IBOutlet weak var isCorrectAnswerImageView: UIImageView!

    func configure(_ model: QuestionDetailModel) {
        if Date().timeIntervalSince1970 - model.date >= 86400 {
            dateLabel.text = Formatters.dateFormatterDefault.string(from: Date(timeIntervalSince1970: model.date))
        } else {
            dateLabel.text = "modified \(Date(timeIntervalSince1970: model.date).timeAgoSinceDate())"
        }
        votesCountLabel.text = String(model.score ?? 0)
        nameLabel.text = model.name
        isCorrectAnswerImageView.isHidden = !model.isAccepted
        guard let body = model.body else { return }
        bodyTextView.attributedText = setQuestionBodyText(html: body)
        contentView.backgroundColor = model.isAnswer ? ProjectColor.answersCell : ProjectColor.questionsCell
    }

    func setQuestionBodyText(html: String) -> NSAttributedString? {
        guard let data = html.data(using: String.Encoding.utf16, allowLossyConversion: false) else {
            return nil
        }

        let regex = try! NSRegularExpression(pattern:"<code>(.*?)</code>", options: [])
        let tmp = html as NSString
        var results = [String]()
        regex.enumerateMatches(in: html, options: [], range: NSMakeRange(0, tmp.length)) { result, flags, stop in
            if let range = result?.range(at: 1) {
                results.append(tmp.substring(with: range))
            }
        }

        guard var attributedString = try? NSMutableAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) else {
            return nil
        }

        let font = UIFont.monospacedDigitSystemFont(ofSize: 13, weight: .regular)
        attributedString = attributedString.string.attributedStringWithFontAndColor(results, color: .white, font: font) as! NSMutableAttributedString
        return attributedString
    }
}
