//
//  QuestionsListViewModel.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 04.09.2021.
//

import RxSwift
import RxCocoa

final class QuestionsListViewModel {

    // MARK: - Public Properties

    private(set) var dataItems = PublishRelay<[Question]>()
    let loadFinished = PublishRelay<Void>()
    let onSelectedQuestion = PublishRelay<Int>()
    let bag = DisposeBag()

    // MARK: - Private properties

    private let service: QuestionsServiceProtocol

    // MARK: - Initializers

    init(service: QuestionsServiceProtocol) {
        self.service = service
    }

    // MARK: - Public Methods

    func getQuestionsList(pageSize: Int, tag: String) {
        service.getQuestions(pageSize: String(pageSize), tag: tag)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.dataItems.accept(response.items)
                    self?.loadFinished.accept(())
                case .error(let error):
                    self?.loadFinished.accept(())
                    print("Get questions list error: \(error.localizedDescription)")
                }
            }
            .disposed(by: bag)
    }
}
