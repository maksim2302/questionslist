//
//  String + AttributedFontAndColor.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 13.09.2021.
//

import UIKit

extension String {
    func attributedStringWithFontAndColor(_ strings: [String], color: UIColor, font: UIFont, characterSpacing: UInt? = nil) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        for string in strings {
            let range = (self as NSString).range(of: string)
            attributedString.addAttribute(NSAttributedString.Key.backgroundColor, value: color, range: range)
            attributedString.addAttribute(NSAttributedString.Key.font, value: font, range: range)
        }
        guard let characterSpacing = characterSpacing else {return attributedString}
        attributedString.addAttribute(NSAttributedString.Key.kern, value: characterSpacing, range: NSRange(location: 0, length: attributedString.length))
        return attributedString
    }
}
