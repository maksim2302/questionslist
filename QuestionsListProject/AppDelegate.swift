//
//  AppDelegate.swift
//  QuestionsListProject
//
//  Created by Maxim Drachev on 02.09.2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var questionsCoordinator = QuestionsCoordinator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = questionsCoordinator.start()
        window?.makeKeyAndVisible()
        return true
    }
}

